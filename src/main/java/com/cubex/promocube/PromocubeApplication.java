package com.cubex.promocube;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class PromocubeApplication implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(PromocubeApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Override
	public void run(String... args) {}
}
