package com.cubex.promocube.core.processing.purchase;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;

import java.util.ArrayList;
import java.util.List;

public class LoyaltyProgramAlgorithm extends DiscountPercentSummaAlgorithm{
    @Override
    public boolean canHandleRequest(AlgorithmRequest request) {
        List<ActionMechanicsType> actionMechanicsTypes = new ArrayList<>();
        actionMechanicsTypes.add(ActionMechanicsType.LOYALTY_PROGRAM);

        if(!actionMechanicsTypes.contains(request.getMechanicsType())) return false;

        if(!this.canHandleRequestPrototype(request)) return false;

        return true;
    }
}
