package com.cubex.promocube.core.processing.actionsettings;

import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;
import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValueStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor

public class AlgorithmRequest {
    private ActionSettings actionSettings;
    private List<PriceListValue> priceListValues;
    private List<Product> products;
    private List<ProductUnionValue> productUnionValues;

    public ActionMechanicsType getMechanicsType(){
        return actionSettings.getMechanicsType();
    }

    public Optional<PriceListValue> getPriceListValue(PriceList priceList, Product product){
        Optional<PriceListValue> optionalPriceListValue = priceListValues.stream()
                .filter(x->x.getPriceList().equals(priceList) &&
                        x.getProduct().equals(product)).findFirst();

        return optionalPriceListValue;
    }

    public List<ProductUnionValue> getProductUnionValues(Product product){
        List<ProductUnionValue> productUnionValueList = productUnionValues.stream()
                .filter(x-> x.getProduct().equals(product) && x.getStatus() == ProductUnionValueStatus.ACTIVATED)
                .collect(Collectors.toList());

        return productUnionValueList;
    }
}
