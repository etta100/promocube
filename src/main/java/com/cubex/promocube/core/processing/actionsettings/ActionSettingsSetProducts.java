package com.cubex.promocube.core.processing.actionsettings;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCondition;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsProduct;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.database.entity.productUnion.ProductUnionValue;

import java.util.List;

public class ActionSettingsSetProducts extends AlgorithmPrototype{

    @Override
    void execute(AlgorithmRequest request) {
        ActionSettings setting = request.getActionSettings();
        List<Product> products = request.getProducts();

        for(ActionSettingsCondition actionSettingsCondition : setting.getConditions()) {
            for (Product product : products) {
                checkGroupAndSetProductToCondition(actionSettingsCondition, product, product.getProductGroup());
            }
        }

        for(ActionSettingsCondition actionSettingsCondition : setting.getConditions()) {
            for (Product product : products) {
                List<ProductUnionValue> productUnionValues = request.getProductUnionValues(product);
                checkGroupAndSetProductToCondition(actionSettingsCondition, product, productUnionValues);
            }
        }
    }

    void checkGroupAndSetProductToCondition(ActionSettingsCondition actionSettingsCondition, Product product, ProductGroup productGroup){
        if (productGroup == null || actionSettingsCondition.isProductContains(product)) return;

        if(actionSettingsCondition.isProductGroupContains(productGroup)) {
            actionSettingsCondition.getProducts().add(new ActionSettingsProduct(product, actionSettingsCondition));
        }

        checkGroupAndSetProductToCondition(actionSettingsCondition, product, productGroup.getProductGroup());
    }

    void checkGroupAndSetProductToCondition(ActionSettingsCondition actionSettingsCondition, Product product, List<ProductUnionValue> productUnionValues){
        if (actionSettingsCondition.isProductContains(product)) return;

        for(ProductUnionValue productUnionValue : productUnionValues){
            if(actionSettingsCondition.isProductUnionContains(productUnionValue.getProductUnion())) {
                actionSettingsCondition.getProducts().add(new ActionSettingsProduct(product, actionSettingsCondition));
            }
        }
    }
}
