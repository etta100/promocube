package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PurchaseProductRepo extends CrudRepository<PurchaseProduct, Long>, PagingAndSortingRepository<PurchaseProduct, Long> {
    Optional<PurchaseProduct> findById(Long id);
}
