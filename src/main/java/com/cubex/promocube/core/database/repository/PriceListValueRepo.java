package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PriceListValueRepo extends CrudRepository<PriceListValue, Long>, PagingAndSortingRepository<PriceListValue, Long> {
    Optional<PriceListValue> findById(Long id);

    //@Query("SELECT u FROM PriceListValue u WHERE u.dateBegin <= :period")
    //@Query(value = "SELECT * FROM price_list_value u WHERE u.date_begin <= :period and u.product_id IN (:productsId)",
    @Query(nativeQuery = true, value = "SELECT * FROM price_list_value u " +
    "INNER JOIN (SELECT u.product_id as product_id, MAX(u.date_begin) as date_begin FROM price_list_value u WHERE u.date_begin <= :period and u.product_id IN (:productsId) GROUP BY u.product_id) as tb " +
    "ON u.date_begin = tb.date_begin AND u.product_id = tb.product_id")
    List<PriceListValue> getPriceListValues(Date period, List<Long> productsId);
}
