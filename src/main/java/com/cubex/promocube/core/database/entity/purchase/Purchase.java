package com.cubex.promocube.core.database.entity.purchase;

import com.cubex.promocube.core.database.entity.department.Department;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.database.entity.user.UserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date date;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<PurchaseProduct> products;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<PurchaseCoupon> coupons = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<PurchaseDiscount> discounts = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<PurchaseCouponPromo> couponPromos = new ArrayList<>();

    @ManyToOne
    private UserRole userRole;

    @ManyToOne
    private Payment payment;

    @ManyToOne
    private Department department;

    public Purchase(Long id) {
        this.id = id;
    }
}
