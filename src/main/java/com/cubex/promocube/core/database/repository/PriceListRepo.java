package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.database.entity.user.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PriceListRepo extends CrudRepository<PriceList, Long>, PagingAndSortingRepository<PriceList, Long> {
    Optional<PriceList> findById(Long id);
}
