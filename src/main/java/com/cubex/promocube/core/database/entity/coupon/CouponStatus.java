package com.cubex.promocube.core.database.entity.coupon;

public enum CouponStatus {
    NONE,
    ACTIVATED,
    USED,
    CANCELED;

    public boolean isActive(){
        return (this==ACTIVATED);
    }
}
