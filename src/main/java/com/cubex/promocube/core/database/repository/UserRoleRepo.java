package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.user.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRoleRepo extends CrudRepository<UserRole, Long>, PagingAndSortingRepository<UserRole, Long> {
    Optional<UserRole> findById(Long id);
}
