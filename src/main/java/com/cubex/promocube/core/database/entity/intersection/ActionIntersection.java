package com.cubex.promocube.core.database.entity.intersection;

import com.cubex.promocube.core.database.entity.action.Action;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class ActionIntersection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    Action action;

    @ManyToOne
    Action actionCross;

    @Enumerated(EnumType.STRING)
    private ActionIntersectionType intersectionType;

    public ActionIntersection(Long id) {
        this.id = id;
    }

    public boolean isActivatedALL(Action action){
        if(this.action.equals(action) && actionCross==null &&
                intersectionType.equals(ActionIntersectionType.ACTIVATED_ALL)){
            return true;
        }
        return false;
    }

    public boolean isDeactivated(Action action){
        if(actionCross==null) return false;

        if(this.action.equals(action) && !actionCross.equals(action) &&
                intersectionType.equals(ActionIntersectionType.DEACTIVATED)){
            return true;
        }
        return false;
    }

    public boolean isActivated(Action action){
        if(actionCross==null) return false;

        if((this.action.equals(action) || actionCross.equals(action)) &&
                intersectionType.equals(ActionIntersectionType.ACTIVATED)){
            return true;
        }
        return false;
    }

    public boolean isApplying(Action action){
        return (this.action!=null && (this.action.equals(action)|| actionCross==null || actionCross.equals(action)));
    }
}
