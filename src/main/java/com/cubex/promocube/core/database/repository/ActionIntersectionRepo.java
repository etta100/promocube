package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ActionIntersectionRepo extends CrudRepository<ActionIntersection, Long>, PagingAndSortingRepository<ActionIntersection, Long> {
    Optional<ActionIntersection> findById(Long id);
}
