package com.cubex.promocube.core.database.entity.purchase;

import com.cubex.promocube.core.database.entity.product.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@ToString

@Entity
public class PurchaseProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long extId;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column
    private BigDecimal priceBase;

    @Column
    private BigDecimal price;

    @Column
    private BigDecimal count;

    @Column
    private BigDecimal sum;

    @Column
    private BigDecimal bonus;

    public void updatePrice(BigDecimal price){
        this.price = price;
        this.sum = this.price.multiply(this.count);
    }
}
