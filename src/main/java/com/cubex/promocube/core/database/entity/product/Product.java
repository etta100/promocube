package com.cubex.promocube.core.database.entity.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    public Product(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductGroup productGroup;
}
