package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.product.ProductGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductGroupRepo extends CrudRepository<ProductGroup, Long>, PagingAndSortingRepository<ProductGroup, Long> {

}
