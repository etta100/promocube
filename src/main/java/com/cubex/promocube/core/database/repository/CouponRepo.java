package com.cubex.promocube.core.database.repository;

import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CouponRepo extends CrudRepository<Coupon, Long>, PagingAndSortingRepository<Coupon, Long> {
    Optional<Coupon> findById(Long id);

    Optional<Coupon> findByName(String name);

    Optional<Coupon> findByIdAndCouponStatus(Long id, CouponStatus couponStatus);


}
