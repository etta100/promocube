package com.cubex.promocube.core.database.entity.actionsettings;

public enum OperationType {
    PRICE,
    PERCENT,
    SUMMA
}
