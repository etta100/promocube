package com.cubex.promocube.core.database.entity.intersection;

public enum ActionIntersectionType {
    NONE,
    ACTIVATED,
    ACTIVATED_ALL,
    DEACTIVATED
}



