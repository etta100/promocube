package com.cubex.promocube.core.database.entity.pricing;

import com.cubex.promocube.core.database.entity.product.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter

@Entity
public class PriceListValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date dateBegin;

    @ManyToOne
    private PriceList priceList;

    @ManyToOne
    private Product product;

    @Column
    private BigDecimal discountValue;
}
