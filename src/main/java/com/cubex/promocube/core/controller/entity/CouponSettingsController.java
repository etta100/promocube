package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.CouponSettingsFacade;
import com.cubex.promocube.core.dto.coupon.CouponSettingsDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api
@RestController
@RequestMapping("/api/couponSettings")
public class CouponSettingsController {
    private CouponSettingsFacade facade;

    public CouponSettingsController(CouponSettingsFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid CouponSettingsDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CouponSettingsDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }
}
