package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ActionSettingsFacade;
import com.cubex.promocube.core.domain.facade.PurchaseFacade;
import com.cubex.promocube.core.dto.purchase.PurchaseDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api
@RestController
@RequestMapping("/api/purchase")
public class PurchaseController {
    private PurchaseFacade facade;

    public PurchaseController(PurchaseFacade purchaseFacade) {
        this.facade = purchaseFacade;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }
}
