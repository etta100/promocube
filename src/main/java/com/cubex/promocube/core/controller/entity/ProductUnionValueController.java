package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.core.domain.facade.ProductUnionValueFacade;
import com.cubex.promocube.core.dto.list.ProductListRequestDto;
import com.cubex.promocube.core.dto.productUnion.ProductUnionValueDto;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/productUnionValue")
public class ProductUnionValueController {
    private ProductUnionValueFacade facade;

    public ProductUnionValueController(ProductUnionValueFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ProductUnionValueDto dto){
        facade.create(dto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductUnionValueDto getById(@PathVariable Long id){
        return facade.findById(id).get();
    }

    @PostMapping("/current")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductUnionValueDto> getPriceListValues(@RequestBody @Valid ProductListRequestDto dto){
        return facade.getValues(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id){
        facade.delete(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductUnionValueDto> selectAll(){
        return facade.selectAll();
    }
}
