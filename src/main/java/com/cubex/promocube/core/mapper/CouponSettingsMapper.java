package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.dto.coupon.CouponSettingsDto;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class CouponSettingsMapper  extends AbstractMapper<CouponSettings, CouponSettingsDto>{
    private final ModelMapper mapper;
    private final ActionService actionService;

    public CouponSettingsMapper(ModelMapper mapper, ActionService actionService) {
        super(CouponSettings.class, CouponSettingsDto.class);
        this.mapper = mapper;
        this.actionService = actionService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(CouponSettingsDto.class, CouponSettings.class)
                .addMappings(m -> m.skip(CouponSettings::setAction))
                .setPostConverter(toEntityConverter());

        mapper.createTypeMap(CouponSettings.class, CouponSettingsDto.class)
                .addMappings(m -> m.skip(CouponSettingsDto::setActionId))
                .setPostConverter(toDtoConverter());
    }

    public CouponSettings toEntity(CouponSettingsDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, CouponSettings.class);
    }

    public CouponSettingsDto toDto(CouponSettings action) {
        return Objects.isNull(action)?null:mapper.map(action,CouponSettingsDto.class);
    }

    @Override
    void mapSpecificFieldsEntity(CouponSettingsDto source, CouponSettings destination) {
        if (source.getActionId() != null) {
            destination.setAction(actionService.findById(source.getActionId()).orElse(null));
        }
    }

    @Override
    void mapSpecificFieldsDto(CouponSettings source, CouponSettingsDto destination) {
        if (source.getAction() != null) {
            destination.setActionId(source.getAction().getId());
        }
    }
}
