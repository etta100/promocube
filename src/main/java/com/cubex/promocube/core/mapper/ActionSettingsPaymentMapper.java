package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsPayment;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsUserRole;
import com.cubex.promocube.core.domain.service.PaymentService;
import com.cubex.promocube.core.domain.service.UserRoleService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsPaymentDto;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsUserRoleDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class ActionSettingsPaymentMapper extends AbstractMapper<ActionSettingsPayment, ActionSettingsPaymentDto>{
    private final ModelMapper mapper;
    private final PaymentService paymentService;

    @Autowired
    public ActionSettingsPaymentMapper(ModelMapper mapper, PaymentService paymentService) {
        super(ActionSettingsPayment.class, ActionSettingsPaymentDto.class);
        this.mapper = mapper;
        this.paymentService = paymentService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsPayment.class, ActionSettingsPaymentDto.class)
                .addMappings(m -> m.skip(ActionSettingsPaymentDto::setPaymentId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsPaymentDto.class, ActionSettingsPayment.class)
                .addMappings(m -> m.skip(ActionSettingsPayment::setPayment))
                .setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsPaymentDto source, ActionSettingsPayment destination) {
        destination.setPayment(paymentService.findById(source.getPaymentId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsPayment source, ActionSettingsPaymentDto destination) {
        destination.setPaymentId(paymentService.getId(source.getPayment()));
    }
}
