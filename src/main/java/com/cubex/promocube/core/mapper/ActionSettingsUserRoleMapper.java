package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsUserRole;
import com.cubex.promocube.core.domain.service.UserRoleService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsUserRoleDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class ActionSettingsUserRoleMapper extends AbstractMapper<ActionSettingsUserRole, ActionSettingsUserRoleDto>{
    private final ModelMapper mapper;
    private final UserRoleService userRoleService;

    @Autowired
    public ActionSettingsUserRoleMapper(ModelMapper mapper, UserRoleService service) {
        super(ActionSettingsUserRole.class, ActionSettingsUserRoleDto.class);
        this.mapper = mapper;
        this.userRoleService = service;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsUserRole.class, ActionSettingsUserRoleDto.class)
                .addMappings(m -> m.skip(ActionSettingsUserRoleDto::setUserRoleId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsUserRoleDto.class, ActionSettingsUserRole.class)
                .addMappings(m -> m.skip(ActionSettingsUserRole::setUserRole)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsUserRoleDto source, ActionSettingsUserRole destination) {
        destination.setUserRole(userRoleService.findById(source.getUserRoleId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsUserRole source, ActionSettingsUserRoleDto destination) {
        destination.setUserRoleId(userRoleService.getId(source.getUserRole()));
    }
}
