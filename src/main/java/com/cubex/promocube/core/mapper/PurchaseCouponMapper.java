package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.domain.service.CouponService;
import com.cubex.promocube.core.dto.purchase.PurchaseCouponDto;
import com.cubex.promocube.core.exceptions.CouponNotFoundException;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class PurchaseCouponMapper extends AbstractMapper<PurchaseCoupon, PurchaseCouponDto>{
    private final ModelMapper mapper;
    private final CouponService couponService;

    @Autowired
    public PurchaseCouponMapper(ModelMapper mapper, CouponService couponService) {
        super(PurchaseCoupon.class, PurchaseCouponDto.class);
        this.mapper = mapper;
        this.couponService = couponService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(PurchaseCoupon.class, PurchaseCouponDto.class)
                .addMappings(m -> m.skip(PurchaseCouponDto::setCoupon))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(PurchaseCouponDto.class, PurchaseCoupon.class)
                .addMappings(m -> m.skip(PurchaseCoupon::setCoupon))
                .setPostConverter(toEntityConverter());

        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

    }

    @Override
    void mapSpecificFieldsEntity(PurchaseCouponDto source, PurchaseCoupon destination) {
        if (source.getCoupon() != null) {
            destination.setCoupon(couponService.findByName(source.getCoupon()).orElse(null));
        }

        if(destination.getCoupon()==null){
            throw new CouponNotFoundException(source.getCoupon());
        }
    }

    void mapSpecificFieldsDto(PurchaseCoupon source, PurchaseCouponDto destination) {
        if (source.getCoupon() != null) {
            destination.setCoupon(source.getCoupon().getName());
        }
    }
}
