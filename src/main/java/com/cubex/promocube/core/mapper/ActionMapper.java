package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.dto.action.ActionDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ActionMapper {
    private final ModelMapper mapper;

    public ActionMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Action toEntity(ActionDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, Action.class);
    }

    public ActionDto toDto(Action action) {
        return Objects.isNull(action)?null:mapper.map(action,ActionDto.class);
    }

}
