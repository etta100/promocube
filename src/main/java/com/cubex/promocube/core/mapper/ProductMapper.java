package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.product.ProductGroup;
import com.cubex.promocube.core.domain.service.ProductGroupService;
import com.cubex.promocube.core.dto.product.ProductDto;
import com.cubex.promocube.core.dto.product.ProductGroupDto;
import com.cubex.promocube.core.exceptions.IdNotFoundMappingException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.Optional;

@Component
public class ProductMapper extends AbstractMapper<Product, ProductDto>{
    private final ModelMapper mapper;
    private final ProductGroupService productGroupService;

    public ProductMapper(ModelMapper mapper, ProductGroupService productGroupService) {
        super(Product.class, ProductDto.class);
        this.mapper = mapper;
        this.productGroupService = productGroupService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Product.class, ProductDto.class)
                .addMappings(m -> m.skip(ProductDto::setProductGroupId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(ProductDto.class, Product.class)
                .addMappings(m -> m.skip(Product::setProductGroup)).setPostConverter(toEntityConverter());
    }

    public Product toEntity(ProductDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto,Product.class);
    }

    public ProductDto toDto(Product product) {
        return Objects.isNull(product)?null:mapper.map(product,ProductDto.class);
    }

    @Override
    void mapSpecificFieldsEntity(ProductDto source, Product destination) {
        if (source.getProductGroupId() != null){
            destination.setProductGroup(productGroupService.findById(source.getProductGroupId()).orElse(null));
            if (destination.getProductGroup() == null){
                throw new IdNotFoundMappingException(source.getProductGroupId(),"productGroupId");
            }
        }
    }

    @Override
    void mapSpecificFieldsDto(Product source, ProductDto destination) {
        Optional.ofNullable(source.getProductGroup())
                .ifPresent(x -> destination.setProductGroupId(x.getId()));
    }
}
