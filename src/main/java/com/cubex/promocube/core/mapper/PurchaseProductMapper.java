package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.domain.service.ProductService;
import com.cubex.promocube.core.dto.purchase.PurchaseProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class PurchaseProductMapper  extends AbstractMapper<PurchaseProduct, PurchaseProductDto>{
    private final ModelMapper mapper;
    private final ProductService productService;

    @Autowired
    public PurchaseProductMapper(ModelMapper mapper, ProductService productService) {
        super(PurchaseProduct.class, PurchaseProductDto.class);
        this.mapper = mapper;
        this.productService = productService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(PurchaseProduct.class, PurchaseProductDto.class)
                .addMappings(m -> m.skip(PurchaseProductDto::setProductId)).setPostConverter(toDtoConverter());

        mapper.createTypeMap(PurchaseProductDto.class, PurchaseProduct.class)
                .addMappings(m -> m.skip(PurchaseProduct::setProduct)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(PurchaseProductDto source, PurchaseProduct destination) {
        destination.setProduct(productService.findById(source.getProductId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(PurchaseProduct source, PurchaseProductDto destination) {
        destination.setProductId(getId(source.getProduct()));
    }

    private Long getId(Product product) {
        return Objects.isNull(product) ? null : product.getId();
    }
}
