package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.department.Department;
import com.cubex.promocube.core.dto.action.ActionDto;
import com.cubex.promocube.core.dto.department.DepartmentDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DepartmentMapper {
    private final ModelMapper mapper;

    public DepartmentMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Department toEntity(DepartmentDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, Department.class);
    }

    public DepartmentDto toDto(Department department) {
        return Objects.isNull(department)?null:mapper.map(department,DepartmentDto.class);
    }

}
