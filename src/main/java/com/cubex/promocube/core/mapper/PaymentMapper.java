package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.dto.payment.PaymentDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PaymentMapper {
    private final ModelMapper mapper;

    public PaymentMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Payment toEntity(PaymentDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, Payment.class);
    }

    public PaymentDto toDto(Payment action) {
        return Objects.isNull(action)?null:mapper.map(action,PaymentDto.class);
    }
}
