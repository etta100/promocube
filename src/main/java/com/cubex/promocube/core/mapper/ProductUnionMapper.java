package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.product.ProductUnion;
import com.cubex.promocube.core.dto.productUnion.ProductUnionDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ProductUnionMapper {
    private final ModelMapper mapper;

    public ProductUnionMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public ProductUnion toEntity(ProductUnionDto dto){
        return Objects.isNull(dto)?null:mapper.map(dto, ProductUnion.class);
    }

    public ProductUnionDto toDto(ProductUnion productUnion) {
        return Objects.isNull(productUnion)?null:mapper.map(productUnion,ProductUnionDto.class);
    }
}
