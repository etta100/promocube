package com.cubex.promocube.core.mapper;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsCouponPromo;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettingsPayment;
import com.cubex.promocube.core.domain.service.CouponSettingsService;
import com.cubex.promocube.core.domain.service.PaymentService;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsCouponPromoDto;
import com.cubex.promocube.core.dto.actionsettigs.ActionSettingsPaymentDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class ActionSettingsCouponPromoMapper extends AbstractMapper<ActionSettingsCouponPromo, ActionSettingsCouponPromoDto>{
    private final ModelMapper mapper;
    private final CouponSettingsService service;

    @Autowired
    public ActionSettingsCouponPromoMapper(ModelMapper mapper, CouponSettingsService paymentService) {
        super(ActionSettingsCouponPromo.class, ActionSettingsCouponPromoDto.class);
        this.mapper = mapper;
        this.service = paymentService;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(ActionSettingsCouponPromo.class, ActionSettingsCouponPromoDto.class)
                .addMappings(m -> m.skip(ActionSettingsCouponPromoDto::setCouponSettingsId))
                .setPostConverter(toDtoConverter());

        mapper.createTypeMap(ActionSettingsCouponPromoDto.class, ActionSettingsCouponPromo.class)
                .addMappings(m -> m.skip(ActionSettingsCouponPromo::setCouponSettings))
                .setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFieldsEntity(ActionSettingsCouponPromoDto source, ActionSettingsCouponPromo destination) {
        destination.setCouponSettings(service.findById(source.getCouponSettingsId()).orElse(null));
    }

    @Override
    void mapSpecificFieldsDto(ActionSettingsCouponPromo source, ActionSettingsCouponPromoDto destination) {
        destination.setCouponSettingsId(service.getId(source.getCouponSettings()));
    }
}
