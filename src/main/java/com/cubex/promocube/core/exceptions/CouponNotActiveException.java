package com.cubex.promocube.core.exceptions;

import com.cubex.promocube.core.database.entity.coupon.Coupon;

public class CouponNotActiveException extends RuntimeException {
    public CouponNotActiveException(Coupon coupon) {
        super("Coupon '"+coupon.getName()+"' not active");
    }
}
