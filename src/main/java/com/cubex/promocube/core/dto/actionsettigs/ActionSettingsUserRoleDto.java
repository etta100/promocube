package com.cubex.promocube.core.dto.actionsettigs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter

@ApiModel
public class ActionSettingsUserRoleDto {
    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long userRoleId;
}
