package com.cubex.promocube.core.dto.purchase;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.DiscountPayoutType;
import com.cubex.promocube.core.database.entity.product.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@ToString

@ApiModel
public class PurchaseDiscountDto {
    @ApiModelProperty(example = "1", hidden = true)
    @NotNull
    private Long id;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long actionSettingsId;

    @NotNull
    @ApiModelProperty(example = "1", required = true)
    private Long purchaseProductId;

    @NotNull
    @ApiModelProperty(example = "500", required = true)
    private BigDecimal sum;

    @NotNull
    @ApiModelProperty(example = "MANY", required = true)
    private DiscountPayoutType discountPayoutType;
}
