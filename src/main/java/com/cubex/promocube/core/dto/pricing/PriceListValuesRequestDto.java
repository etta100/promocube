package com.cubex.promocube.core.dto.pricing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter

@ApiModel
public class PriceListValuesRequestDto {
    @NotNull
    @ApiModelProperty(example = "2021-07-01", required = true)
    private Date period;

    List<Long> productsId;
}
