package com.cubex.promocube.core.dto.list;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter

@ApiModel
public class ProductListRequestDto {
    @NotNull
    @ApiModelProperty(example = "2021-07-01", required = true)
    private Date period;

    List<Long> productsId;
}
