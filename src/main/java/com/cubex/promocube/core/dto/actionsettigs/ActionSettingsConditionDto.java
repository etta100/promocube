package com.cubex.promocube.core.dto.actionsettigs;

import com.cubex.promocube.core.database.entity.actionsettings.OperationType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter

@ApiModel
public class ActionSettingsConditionDto {
    @ApiModelProperty(example = "SUMMA", required = true)
    @NotNull
    private OperationType operationType;

    @ApiModelProperty(example = "10", required = true)
    @NotNull
    private BigDecimal discountValue;

    List<ActionSettingsProductDto> products;

    List<ActionSettingsProductGroupDto> productGroups;

    List<ActionSettingsProductUnionDto> productUnions;
}
