package com.cubex.promocube.core.dto.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@ApiModel
public class ProductGroupDto {
    @ApiModelProperty(example = "1", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(example = "test", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(example = "1", required = true)
    private Long productGroupId;
}
