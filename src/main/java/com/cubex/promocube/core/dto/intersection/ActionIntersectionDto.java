package com.cubex.promocube.core.dto.intersection;

import com.cubex.promocube.core.database.entity.intersection.ActionIntersectionType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@ApiModel

public class ActionIntersectionDto {
    @NotNull
    @ApiModelProperty(example = "1", required = true, position = 1)
    private Long id;

    @NotNull
    @ApiModelProperty(example = "1", required = true, position = 2)
    Long actionId;

    @ApiModelProperty(example = "1", position = 3)
    Long actionCrossId;

    @NotNull
    @ApiModelProperty(example = "ACTIVATED", position = 4, required = true)
    private ActionIntersectionType intersectionType;

    public ActionIntersectionDto(Long id) {
        this.id = id;
    }
}
