package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.domain.service.ActionService;
import com.cubex.promocube.core.dto.action.ActionDto;
import com.cubex.promocube.core.dto.payment.PaymentDto;
import com.cubex.promocube.core.mapper.ActionMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ActionFacade {
    private ActionService service;
    private ActionMapper mapper;

    public ActionFacade(ActionService service, ActionMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(ActionDto dto) {
        Optional<ActionDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<ActionDto> findById(Long id){
        Optional<Action> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public List<ActionDto> findAll() {
        return StreamSupport.stream(service.findAll().spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
