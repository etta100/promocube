package com.cubex.promocube.core.domain.facade;

import com.cubex.promocube.core.database.entity.user.UserRole;
import com.cubex.promocube.core.domain.service.UserRoleService;
import com.cubex.promocube.core.dto.user.UserRoleDto;
import com.cubex.promocube.core.mapper.UserRoleMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRoleFacade {
    private UserRoleService service;
    private UserRoleMapper mapper;

    public UserRoleFacade(UserRoleService service, UserRoleMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public void create(UserRoleDto dto) {
        Optional<UserRoleDto> optional = Optional.of(dto);
        service.create(optional.map(mapper::toEntity).get());
    }

    public Optional<UserRoleDto> findById(Long id){
        Optional<UserRole> optional = service.findById(id);
        return optional.map(mapper::toDto);
    }

    public void delete(Long id) {
        service.delete(id);
    }
}
