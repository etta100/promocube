package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.repository.CouponRepo;
import com.cubex.promocube.core.database.repository.PurchaseRepo;
import com.cubex.promocube.core.processing.PurchaseProcessing;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PurchaseService {
    private PurchaseRepo repo;

    public Purchase processing(Purchase purchase) {
        return repo.save(purchase);
    }

    public PurchaseService(PurchaseRepo repo, CouponRepo couponRepo, PurchaseProcessing processing) {
        this.repo = repo;
    }

    public Purchase create(Purchase dto) {
        return repo.save(dto);
    }

    public Optional<Purchase> findById(Long id){
        return  repo.findById(id);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
