package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.repository.ActionIntersectionRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActionIntersectionService {
    private final ActionIntersectionRepo repo;

    public ActionIntersectionService(ActionIntersectionRepo repo) {
        this.repo = repo;
    }

    public void create(ActionIntersection item) {
        repo.save(item);
    }

    public Optional<ActionIntersection> findById(Long id){
        return  repo.findById(id);
    }

    public List<ActionIntersection> findAll(){
        return (List<ActionIntersection>) repo.findAll();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
