package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.pricing.PriceListValue;
import com.cubex.promocube.core.database.repository.PriceListValueRepo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PriceListValueService {
    private PriceListValueRepo repo;

    public PriceListValueService(PriceListValueRepo repo) {
        this.repo = repo;
    }

    public void create(PriceListValue item) {
        repo.save(item);
    }

    public Optional<PriceListValue> findById(Long id){
        return  repo.findById(id);
    }

    public Long getId(PriceListValue item) {
        return Objects.isNull(item) ? null : item.getId();
    }

    public List<PriceListValue> getPriceListValues(Date period,List<Long> productsId){
        return repo.getPriceListValues(period,productsId);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
