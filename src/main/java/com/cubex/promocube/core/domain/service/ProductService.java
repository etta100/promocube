package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.repository.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {
    private ProductRepo repo;

    public ProductService(ProductRepo productRepo) {
        this.repo = productRepo;
    }

    public void create(Product product) {
        repo.save(product);
    }

    public Optional<Product> findById(Long id){
        return  repo.findById(id);
    }

    public Long getId(Product product) {
        return Objects.isNull(product) ? null : product.getId();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public List<Product> findAll() {
        return (List<Product>) repo.findAll();
    }
}
