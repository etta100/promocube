package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.actionsettings.ActionSettings;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.database.repository.PaymentRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PaymentService {
    private PaymentRepo repo;

    public PaymentService(PaymentRepo repo) {
        this.repo = repo;
    }

    public void create(Payment item) {
        repo.save(item);
    }

    public Optional<Payment> findById(Long id){
        return repo.findById(id);
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }

    public Long getId(Payment item) {
        return Objects.isNull(item) ? null : item.getId();
    }

    public List<Payment> findAll() {
        return (List<Payment>) repo.findAll();
    }
}
