package com.cubex.promocube.core.domain.service;

import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.database.repository.PriceListRepo;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class PriceListService {
    private PriceListRepo repo;

    public PriceListService(PriceListRepo repo) {
        this.repo = repo;
    }

    public void create(PriceList item) {
        repo.save(item);
    }

    public Optional<PriceList> findById(Long id){
        return  repo.findById(id);
    }

    public Long getId(PriceList item) {
        return Objects.isNull(item) ? null : item.getId();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
