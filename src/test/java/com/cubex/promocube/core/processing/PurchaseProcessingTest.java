package com.cubex.promocube.core.processing;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.database.entity.action.Action;
import com.cubex.promocube.core.database.entity.action.ActionMechanicsType;
import com.cubex.promocube.core.database.entity.actionsettings.*;
import com.cubex.promocube.core.database.entity.coupon.Coupon;
import com.cubex.promocube.core.database.entity.coupon.CouponSettings;
import com.cubex.promocube.core.database.entity.coupon.CouponStatus;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersection;
import com.cubex.promocube.core.database.entity.intersection.ActionIntersectionType;
import com.cubex.promocube.core.database.entity.payment.Payment;
import com.cubex.promocube.core.database.entity.payment.PaymentType;
import com.cubex.promocube.core.database.entity.pricing.PriceList;
import com.cubex.promocube.core.database.entity.product.Product;
import com.cubex.promocube.core.database.entity.purchase.Purchase;
import com.cubex.promocube.core.database.entity.purchase.PurchaseCoupon;
import com.cubex.promocube.core.database.entity.purchase.PurchaseDiscount;
import com.cubex.promocube.core.database.entity.purchase.PurchaseProduct;
import com.cubex.promocube.core.domain.service.ActionIntersectionService;
import com.cubex.promocube.core.domain.service.ActionSettingsService;
import com.cubex.promocube.core.util.BigDecimalUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class PurchaseProcessingTest {
    @Autowired
    private PurchaseProcessing processing;

    @MockBean
    private ActionSettingsService actionSettingsService;

    @MockBean
    private ActionIntersectionService actionIntersectionService;

    private Date paramDate;

    @BeforeEach
    void init(){
        this.paramDate = new Date();
    }

    ActionSettings makeActionSettings(Long id, Action action, DiscountPayoutType discountPayoutType){
        Calendar calendar = new GregorianCalendar();
        Date dateBegin = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        Date dateEnd = calendar.getTime();
        calendar.clear();

        ActionSettings actionSettings = new ActionSettings();
        actionSettings.setId(id);
        actionSettings.setAction(action);
        actionSettings.setMechanicsType(action.getMechanicsType());

        actionSettings.setDateBegin(dateBegin);
        actionSettings.setDateEnd(dateEnd);

        actionSettings.setUserRoles(new ArrayList<>());
        actionSettings.setIsCouponRequired(false);
        actionSettings.setDiscountFactor(BigDecimal.ZERO);
        actionSettings.setDiscountPayoutType(discountPayoutType);

        return actionSettings;
    }

    ActionSettings makeActionSettings(Long id, Action action, DiscountPayoutType discountPayoutType, ActionSettingsCondition actionSettingsCondition){
        ActionSettings actionSettings = makeActionSettings(id, action, discountPayoutType);
        actionSettings.setConditions(Collections.singletonList(actionSettingsCondition));
        return actionSettings;
    }

    ActionSettingsCondition makeActionSettingsCondition(Long id, Product product, BigDecimal discount, OperationType operationType){
        ActionSettingsCondition actionSettingsCondition = new ActionSettingsCondition(id, operationType, discount);
        ActionSettingsProduct actionSettingsProduct = new ActionSettingsProduct(100L,product,actionSettingsCondition);

        actionSettingsCondition.setProducts(Collections.singletonList(actionSettingsProduct));

        return  actionSettingsCondition;
    }

    Purchase makePurchase(Long id, Date currentDate){
        Purchase purchase = new Purchase(id);
        purchase.setDate(currentDate);
        return purchase;
    }

    Purchase makePurchase(Date currentDate, Product product, BigDecimal price){
        Purchase purchase = new Purchase(100L);
        purchase.setDate(currentDate);

        PurchaseProduct purchaseProduct = makePurchaseProduct(100L, product, price, BigDecimal.ONE);
        purchase.setProducts(Collections.singletonList(purchaseProduct));

        return purchase;
    }

    Purchase makePurchase(Date currentDate, Product product, BigDecimal price, Action action){
        Purchase purchase = makePurchase(currentDate, product, price);

        if(!purchase.getProducts().isEmpty()){
            ActionSettings actionSettings = new ActionSettings(500L);
            actionSettings.setAction(action);
            purchase.getDiscounts().add(new PurchaseDiscount(actionSettings,purchase.getProducts().get(0),DiscountPayoutType.MANY,BigDecimal.TEN));
        }

        return purchase;
    }

    PurchaseProduct makePurchaseProduct(Long id, Product product, BigDecimal price, BigDecimal count){
        PurchaseProduct purchaseProduct = new PurchaseProduct();
        purchaseProduct.setId(id);
        purchaseProduct.setProduct(product);
        purchaseProduct.setPriceBase(price);
        purchaseProduct.setCount(count);
        purchaseProduct.updatePrice(price);
        purchaseProduct.setBonus(BigDecimal.ZERO);
        return purchaseProduct;
    }

    @Test
    public void executeL004(){
        Date currentDate = new Date();

        Action action = new Action(100L,"Director's bonuses",ActionMechanicsType.BONUS_PERCENT_SUMMA);
        Product product = new Product(100L);

        //-------------------------------------------------
        //step 1: bonus 5%
        //-------------------------------------------------
        ActionSettingsCondition condition = makeActionSettingsCondition(100L, product, new BigDecimal(5), OperationType.PERCENT);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.BONUSES, condition);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettings));

        BigDecimal price =  BigDecimal.valueOf(100);
        Purchase purchase = makePurchase(currentDate, product, price);

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        BigDecimal bonusDecimal = BigDecimalUtil.percentage(price,condition.getDiscountValue());
        assertTrue(purchase.getProducts().stream().allMatch(x->x.getBonus().equals(bonusDecimal)));
        assertTrue(purchase.getDiscounts().stream().allMatch(x->x.getSum().equals(bonusDecimal)));

        //-------------------------------------------------
        //step 2: bonus 2000
        //-------------------------------------------------
        ActionSettingsCondition condition2 = makeActionSettingsCondition(200L,product,BigDecimal.valueOf(2000),OperationType.SUMMA);
        ActionSettings actionSettings2000 = makeActionSettings(200L, action, DiscountPayoutType.BONUSES, condition2);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettings2000));

        purchase = makePurchase(currentDate, product, BigDecimal.valueOf(5000));

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        purchase.getProducts().forEach(x->assertEquals(x.getBonus(),condition2.getDiscountValue()));
        purchase.getDiscounts().forEach(x->assertEquals(x.getSum(),condition2.getDiscountValue()));
    }

    @Test
    public void executeL016(){
        Date currentDate = new Date();

        Action action = new Action(100L,"Ночь скидок",ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);
        PriceList priceList = new PriceList(100L);
        Product product = new Product(100L);

        BigDecimal currentPrice = new BigDecimal(120000);
        Purchase purchase = makePurchase(currentDate, product, currentPrice);

        ActionSettingsCondition condition = makeActionSettingsCondition(100L,product,new BigDecimal(100000),OperationType.PRICE);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, condition);
        actionSettings.setPriceList(priceList);

        assertEquals(actionSettings.getConditions().size(),1);
        assertEquals(actionSettings.getConditions().get(0).getProducts().size(),1);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettings));
        assertEquals(actionSettingsService.findAllProcessed(currentDate).size(),1);

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        purchase.getProducts().forEach(x->assertEquals(x.getSum(),condition.getDiscountValue()));
    }

    @Test
    public void executeL018(){
        Calendar calendar = new GregorianCalendar();
        Date currentDate = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        Date dateEnd = calendar.getTime();

        PriceList priceList = new PriceList(100L);
        Product product = new Product(100L);

        Action action = new Action(100L, "Ночь скидок",ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);
        CouponSettings couponSettings = new CouponSettings(100L,"test", action, currentDate, dateEnd);
        Coupon coupon = new Coupon(100L,"test",CouponStatus.ACTIVATED, couponSettings);

        BigDecimal currentPrice = new BigDecimal(120000);
        Purchase purchase = makePurchase(currentDate, product, currentPrice);
        purchase.setCoupons(Collections.singletonList(new PurchaseCoupon(100L, coupon)));

        ActionSettingsCondition condition = makeActionSettingsCondition(100L,product,new BigDecimal(100000),OperationType.PRICE);
        ActionSettings actionSettings = makeActionSettings(100L,action, DiscountPayoutType.MANY,condition);
        actionSettings.setPriceList(priceList);
        actionSettings.setIsCouponRequired(true);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettings));

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getDiscounts().isEmpty());

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        purchase.getProducts().forEach(x->assertEquals(x.getSum(),condition.getDiscountValue()));
        purchase.getDiscounts().forEach(x->assertEquals(x.getSum(),BigDecimal.valueOf(20000)));
    }

    @Test
    public void executeL020(){
        Date currentDate = new Date();

        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        //-------------------------------------------------
        //step 1: bonus 5%
        //-------------------------------------------------
        Purchase purchase = makePurchase(currentDate, product, new BigDecimal(10000));

        ActionSettingsCondition conditionPERCENT = makeActionSettingsCondition(100L, product, new BigDecimal(5), OperationType.PERCENT);
        ActionSettings actionSettingsPERCENT = makeActionSettings(100L, action, DiscountPayoutType.MANY, conditionPERCENT);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettingsPERCENT));

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        purchase.getProducts().forEach(x->assertEquals(x.getSum(),new BigDecimal("9500.00")));
        purchase.getDiscounts().forEach(x->assertEquals(x.getSum(),new BigDecimal("500.00")));

        //-------------------------------------------------
        //step 2: bonus 2000
        //-------------------------------------------------
        purchase = makePurchase(currentDate, product, new BigDecimal(10000));

        ActionSettingsCondition conditionSUM = makeActionSettingsCondition(200L, product, new BigDecimal(2000), OperationType.SUMMA);
        ActionSettings actionSettingsSUM = makeActionSettings(200L, action, DiscountPayoutType.MANY, conditionSUM);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettingsSUM));

        processing.execute(currentDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        purchase.getProducts().forEach(x->assertEquals(x.getSum(),new BigDecimal(8000)));
        purchase.getDiscounts().forEach(x->assertEquals(x.getSum(),conditionSUM.getDiscountValue()));
    }

    @Test
    public void executeL021_ConcurrentPricesCompensation(){
        Date currentDate = new Date();

        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_BY_PRICE_LIST);
        action.setName("Прайсы конкурента");

        PriceList priceList = new PriceList(100L);
        Product product = new Product(100L);

        BigDecimal currentPrice = BigDecimal.valueOf(120000);
        Purchase purchase = makePurchase(currentDate, product, currentPrice);

        ActionSettingsCondition condition = makeActionSettingsCondition(100L,product,BigDecimal.valueOf(100000),OperationType.PRICE);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, condition);
        actionSettings.setPriceList(priceList);
        actionSettings.setDiscountFactor(BigDecimal.valueOf(1.2));

        assertEquals(actionSettings.getConditions().size(),1);
        assertEquals(actionSettings.getConditions().get(0).getProducts().size(),1);

        when(actionSettingsService.findAllProcessed(currentDate)).thenReturn(Collections.singletonList(actionSettings));

        processing.execute(currentDate, purchase);

        BigDecimal discountPrice = condition.getDiscountValue();
        BigDecimal discountFactor = actionSettings.getDiscountFactor();
        BigDecimal discount = currentPrice.subtract(currentPrice.subtract(discountPrice).multiply(discountFactor));

        assertEquals(purchase.getProducts().size(),1);
        purchase.getProducts().forEach(x->assertEquals(x.getSum(),discount));
    }

    @Test
    public void executeL2003_EmptyPurchaseDiscountTHANActionApplyingOK(){
        Action action = new Action(100L, ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.valueOf(10), OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000));

        assertEquals(purchase.getDiscounts().size(),0);

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getDiscounts().size(),1);
        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionRulesDefaultTHAN_ActionApplyingIGNORE(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCross = new Action(200L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        BigDecimal currentPrice = BigDecimal.valueOf(120000);
        Purchase purchase = makePurchase(paramDate, product, currentPrice, actionCross);

        ActionSettingsCondition condition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, condition);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(new ArrayList<>());

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertFalse(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionActionCross_THAN_ActionApplyingOK(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCross = new Action(200L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        ActionIntersection actionIntersection = new ActionIntersection(100L,action, actionCross,ActionIntersectionType.ACTIVATED);

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000), actionCross);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.singletonList(actionIntersection));

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
        assertTrue(purchase.getDiscounts().stream().filter(x->x.getAction().equals(action)).allMatch(x->x.getSum().equals(BigDecimal.TEN)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionAnotherAction_THAN_ActionApplyingIGNORE(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCross = new Action(200L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        ActionIntersection actionIntersection = new ActionIntersection(100L,action,any(Action.class),ActionIntersectionType.ACTIVATED);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.singletonList(actionIntersection));

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000), actionCross);

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertFalse(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionALL_THAN_ActionApplyingOK(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCross = new Action(200L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        ActionIntersection actionIntersection = new ActionIntersection(100L,action,null,ActionIntersectionType.ACTIVATED_ALL);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.singletonList(actionIntersection));

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000), actionCross);

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionRulesDefaultEqualsDeactivatedTHAN_ActionApplyingIGNORE(){
        Action action = new Action(100L);
        Action actionCross = new Action(200L);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        ActionIntersection actionIntersection = new ActionIntersection(100L,action,actionCross,ActionIntersectionType.DEACTIVATED);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.singletonList(actionIntersection));

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000), actionCross);

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertFalse(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeL2003_ExistsPurchaseDiscountANDIntersectionALLWithoutActionCross_THAN_ActionApplyingOK(){
        Action action = new Action(100L);
        Action actionCross = new Action(200L);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        ActionIntersection actionIntersection = new ActionIntersection(100L,action,null,ActionIntersectionType.ACTIVATED_ALL);
        ActionIntersection actionIntersection2 = new ActionIntersection(100L,action,actionCross,ActionIntersectionType.DEACTIVATED);

        List<ActionIntersection> actionIntersections = new ArrayList<>();
        actionIntersections.add(actionIntersection);
        actionIntersections.add(actionIntersection2);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(actionIntersections);

        Purchase purchase = makePurchase(paramDate, product, new BigDecimal(120000), actionCross);

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(actionCross)));
        assertFalse(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    public void EXECUTE_CheckExtraBonusActionApplaying(PriceList priceList){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCross = new Action(200L,ActionMechanicsType.LOYALTY_PROGRAM);
        Product product = new Product(100L);

        BigDecimal currentPrice = BigDecimal.valueOf(120000);
        Purchase purchase = makePurchase(paramDate, product, currentPrice);

        action.setName("CashBack (value from price list)");
        BigDecimal bonuses = BigDecimal.valueOf(30000);
        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, bonuses, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.BONUSES, actionSettingsCondition);
        if(priceList!=null) actionSettings.setPriceList(priceList);

        actionCross.setName("LOYALTY 3.3%");
        ActionSettingsCondition actionSettingsConditionLoyalty = makeActionSettingsCondition(200L, product, BigDecimal.valueOf(3.3), OperationType.PERCENT);
        ActionSettings actionSettingsLoyalty = makeActionSettings(200L, actionCross, DiscountPayoutType.BONUSES, actionSettingsConditionLoyalty);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Arrays.asList(actionSettings, actionSettingsLoyalty));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getDiscounts().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);

        assertTrue(purchase.getDiscounts().stream()
                .allMatch(x->x.getAction().equals(action) &&
                        x.getActionSettings().equals(actionSettings) &&
                        x.getSum().equals(bonuses)));
    }

    @Test
    public void execute7500_CheckExtraBonusActionApplying(){
        EXECUTE_CheckExtraBonusActionApplaying(null);
    }

    @Test
    public void executeL005_CheckExtraBonusActionApplying_WITH_PriceList(){
        PriceList priceList = new PriceList(100L);
        priceList.setName("Cashback bonus by price list");
        EXECUTE_CheckExtraBonusActionApplaying(priceList);
    }

    @Test
    public void executeF7601_CheckActionPaymentsEmpty(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        Purchase purchase = makePurchase(paramDate, product, BigDecimal.valueOf(120000));

        assertTrue(purchase.getDiscounts().isEmpty());
        assertTrue(actionSettings.getPayments().isEmpty());

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().stream().anyMatch(x->x.getAction().equals(action)));
    }

    @Test
    public void executeF7601_CheckActionPayments(){
        Action actionKASPI = new Action(100L, ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionSTANDART = new Action(200L, ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionCASH = new Action(300L, ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        Payment paymentKaspiQRGold = new Payment(100L, "Kaspi QR Gold", PaymentType.CASH);
        Payment paymentCredit = new Payment(200L, "Credit", PaymentType.CREDIT);
        Payment paymentCashMoney = new Payment(300L, "Cash money", PaymentType.CASH);

        ActionSettingsCondition actionSettingsConditionKASPI = makeActionSettingsCondition(100L,product,BigDecimal.valueOf(2.3),OperationType.PERCENT);
        ActionSettings actionSettingsKASPI = makeActionSettings(100L, actionKASPI, DiscountPayoutType.BONUSES, actionSettingsConditionKASPI);
        actionSettingsKASPI.getPayments().add(new ActionSettingsPayment(100L, paymentKaspiQRGold));

        ActionSettingsCondition actionSettingsConditionSTANDART = makeActionSettingsCondition(200L,product,BigDecimal.ONE,OperationType.PERCENT);
        ActionSettings actionSettingsSTANDART = makeActionSettings(200L, actionSTANDART, DiscountPayoutType.BONUSES, actionSettingsConditionSTANDART);
        actionSettingsSTANDART.getPayments().add(new ActionSettingsPayment(200L, paymentKaspiQRGold));
        actionSettingsSTANDART.getPayments().add(new ActionSettingsPayment(201L, paymentCredit));

        ActionSettingsCondition actionSettingsConditionCASH = makeActionSettingsCondition(300L,product,BigDecimal.valueOf(3.3),OperationType.PERCENT);
        ActionSettings actionSettingsCASH = makeActionSettings(300L, actionCASH, DiscountPayoutType.BONUSES, actionSettingsConditionCASH);
        actionSettingsCASH.getPayments().add(new ActionSettingsPayment(300L, paymentCashMoney));

        ActionIntersection actionIntersection  = new ActionIntersection(100L,actionKASPI,actionSTANDART, ActionIntersectionType.ACTIVATED);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Arrays.asList(actionSettingsKASPI, actionSettingsSTANDART,actionSettingsCASH));
        when(actionIntersectionService.findAll()).thenReturn(Collections.singletonList(actionIntersection));

        BigDecimal price = BigDecimal.valueOf(120000);
        BigDecimal bonusKASPI = BigDecimalUtil.percentage(price,new BigDecimal("2.3"));
        BigDecimal bonusSTANDART = BigDecimalUtil.percentage(price,new BigDecimal(1));
        BigDecimal bonusCASH = BigDecimalUtil.percentage(price,new BigDecimal("3.3"));
        Purchase purchase;

        //1:Kaspi 2.5% + 1%
        purchase = makePurchase(paramDate, product, price);
        purchase.setPayment(paymentKaspiQRGold);

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getProducts().stream().allMatch(x->x.getBonus().equals(BigDecimal.ZERO)));
        assertTrue(purchase.getDiscounts().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        purchase.getProducts().forEach(x->assertEquals(x.getBonus(),bonusKASPI.add(bonusSTANDART)));

        assertEquals(purchase.getDiscounts().size(),2);
        assertEquals(purchase.getDiscounts().stream().filter(x->x.getAction().equals(actionKASPI) && x.getSum().equals(bonusKASPI)).count(),1);
        assertEquals(purchase.getDiscounts().stream().filter(x->x.getAction().equals(actionSTANDART) && x.getSum().equals(bonusSTANDART)).count(),1);

        //2:Credit - 1%
        purchase = makePurchase(paramDate, product, price);
        purchase.setPayment(paymentCredit);

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getProducts().stream().allMatch(x->x.getBonus().equals(BigDecimal.ZERO)));
        assertTrue(purchase.getDiscounts().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        purchase.getProducts().forEach(x->assertEquals(x.getBonus(),bonusSTANDART));

        assertEquals(purchase.getDiscounts().size(),1);
        assertTrue(purchase.getDiscounts().stream().allMatch(x->x.getAction().equals(actionSTANDART) && x.getSum().equals(bonusSTANDART)));

        //3: Cash 3.3%
        purchase = makePurchase(paramDate, product, price);
        purchase.setPayment(paymentCashMoney);

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getProducts().stream().allMatch(x->x.getBonus().equals(BigDecimal.ZERO)));
        assertTrue(purchase.getDiscounts().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        purchase.getProducts().forEach(x->assertEquals(x.getBonus(),bonusCASH));

        assertEquals(purchase.getDiscounts().size(),1);
        assertTrue(purchase.getDiscounts().stream().allMatch(x->x.getAction().equals(actionCASH) && x.getSum().equals(bonusCASH)));
    }

    public void EXECUTE_CheckCouponSumValueActionApplaying(PriceList priceList) {
        Calendar calendar = new GregorianCalendar();
        Date dateBegin = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        Date dateEnd = calendar.getTime();

        Action action = new Action(100L, ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);
        Product product2 = new Product(200L);
        Product product3 = new Product(300L);

        ActionSettingsCondition condition = makeActionSettingsCondition(100L,product,new BigDecimal(20000),OperationType.SUMMA);
        ActionSettingsCondition condition2 = makeActionSettingsCondition(100L,product2,new BigDecimal(20000),OperationType.SUMMA);
        ActionSettingsCondition condition3 = makeActionSettingsCondition(100L,product3,new BigDecimal(30000),OperationType.SUMMA);
        ActionSettings actionSettings = makeActionSettings(100L,action, DiscountPayoutType.MANY);
        actionSettings.setConditions(Arrays.asList(condition,condition2,condition3));
        actionSettings.setIsCouponRequired(true);
        if(priceList!=null) actionSettings.setPriceList(priceList);

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        PurchaseProduct purchaseProduct = makePurchaseProduct(100L, product, new BigDecimal(100000), BigDecimal.ONE);
        PurchaseProduct purchaseProduct2 = makePurchaseProduct(200L, product2, new BigDecimal(100000), BigDecimal.ONE);
        PurchaseProduct purchaseProduct3 = makePurchaseProduct(300L, product3, new BigDecimal(100000), BigDecimal.ONE);
        Purchase purchase = makePurchase(100L, paramDate);
        purchase.setProducts(Arrays.asList(purchaseProduct,purchaseProduct2,purchaseProduct3));

        assertEquals(purchase.getProducts().size(),3);
        assertTrue(purchase.getDiscounts().isEmpty());
        assertTrue(purchase.getCoupons().isEmpty());

        processing.execute(paramDate, purchase);

        assertTrue(purchase.getDiscounts().isEmpty());
        assertTrue(purchase.getCoupons().isEmpty());

        CouponSettings couponSettings = new CouponSettings(100L,"test",action,dateBegin,dateEnd);
        Coupon coupon = new Coupon(100L,"12345",CouponStatus.ACTIVATED,couponSettings);
        coupon.setSum(new BigDecimal(50000));

        purchase.getCoupons().add(new PurchaseCoupon(100L, coupon));

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(), 3);

        assertEquals(purchase.getDiscounts().size(), 3);
        assertTrue(purchase.getDiscounts().stream().allMatch(x -> x.getAction().equals(action)));

        assertEquals(purchase.getDiscounts().stream().map(PurchaseDiscount::getSum)
                .reduce(BigDecimal.ZERO, BigDecimal::add), coupon.getSum());

        purchase.getDiscounts().stream().filter(x -> x.getPurchaseProduct().getProduct().equals(product))
                .forEach(x->assertEquals(x.getSum(),new BigDecimal(20000)));
        purchase.getDiscounts().stream().filter(x -> x.getPurchaseProduct().getProduct().equals(product2))
                .forEach(x->assertEquals(x.getSum(),new BigDecimal(20000)));
        purchase.getDiscounts().stream().filter(x -> x.getPurchaseProduct().getProduct().equals(product3))
                .forEach(x->assertEquals(x.getSum(),new BigDecimal(10000)));

        assertEquals(purchase.getCoupons().size(), 1);

        assertTrue(purchase.getCoupons().stream()
                .allMatch(x -> x.getCoupon().equals(coupon) &&
                        x.getActionSettings().equals(actionSettings) &&
                        x.getApplying()));
    }

    @Test
    public void executeF8000_CheckCouponSumValueActionApplaying() {
        EXECUTE_CheckCouponSumValueActionApplaying(null);
    }

    @Test
    public void executeF8001_CheckCouponSumValueActionApplaying_WITH_PriceList(){
        PriceList priceList = new PriceList(100L);
        priceList.setName("Discount by price list");
        EXECUTE_CheckCouponSumValueActionApplaying(null);
    }

    @Test
    public void executeF8002_CheckCreateCoupon_BY_ActionSettings_EmptyCouponSumma(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionNext = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        CouponSettings couponSettings = new CouponSettings();
        couponSettings.setId(100L);
        couponSettings.setAction(actionNext);

        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettingsCouponPromo actionSettingsCouponPromo = new ActionSettingsCouponPromo(100L,couponSettings);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);
        actionSettings.setCouponPromos(Collections.singletonList(actionSettingsCouponPromo));

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        Purchase purchase = makePurchase(paramDate, product, BigDecimal.valueOf(120000));

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getDiscounts().isEmpty());
        assertTrue(purchase.getCouponPromos().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);
        assertEquals(purchase.getCouponPromos().size(),1);

        assertNull(actionSettingsCouponPromo.getSum());
        purchase.getCouponPromos().forEach(x->assertNull(x.getCoupon().getSum()));
    }

    @Test
    public void executeF8002_CheckCreateCoupon_BY_ActionSettings_CouponSummaIsNotEmpty(){
        Action action = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Action actionNext = new Action(100L,ActionMechanicsType.DISCOUNT_PERCENT_SUMMA);
        Product product = new Product(100L);

        CouponSettings couponSettings = new CouponSettings();
        couponSettings.setId(100L);
        couponSettings.setAction(actionNext);

        BigDecimal couponSum = new BigDecimal(500);
        ActionSettingsCondition actionSettingsCondition = makeActionSettingsCondition(100L, product, BigDecimal.TEN, OperationType.SUMMA);
        ActionSettingsCouponPromo actionSettingsCouponPromo = new ActionSettingsCouponPromo(100L,couponSettings,couponSum);
        ActionSettings actionSettings = makeActionSettings(100L, action, DiscountPayoutType.MANY, actionSettingsCondition);
        actionSettings.setCouponPromos(Collections.singletonList(actionSettingsCouponPromo));

        when(actionSettingsService.findAllProcessed(paramDate)).thenReturn(Collections.singletonList(actionSettings));
        when(actionIntersectionService.findAll()).thenReturn(Collections.EMPTY_LIST);

        Purchase purchase = makePurchase(paramDate, product, BigDecimal.valueOf(120000));

        assertEquals(purchase.getProducts().size(),1);
        assertTrue(purchase.getDiscounts().isEmpty());
        assertTrue(purchase.getCouponPromos().isEmpty());

        processing.execute(paramDate, purchase);

        assertEquals(purchase.getProducts().size(),1);
        assertEquals(purchase.getDiscounts().size(),1);
        assertEquals(purchase.getCouponPromos().size(),1);

        assertEquals(actionSettingsCouponPromo.getSum(),couponSum);
        purchase.getCouponPromos().forEach(x->assertEquals(x.getCoupon().getSum(),couponSum));
    }
}