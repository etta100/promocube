package com.cubex.promocube.core.controller.entity;

import com.cubex.promocube.PromocubeApplication;
import com.cubex.promocube.core.domain.facade.ActionFacade;
import com.cubex.promocube.core.dto.action.ActionDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = {PromocubeApplication.class})
@ActiveProfiles("test")
class ActionControllerTest {
    @Autowired
    private ActionController controller;

    @MockBean
    private ActionFacade facade;

    private final String path = "/api/action";
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void create() throws Exception {
        MockHttpServletRequestBuilder builder = post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"id\": 15,\n" +
                        "  \"mechanicsType\": 1,\n" +
                        "  \"name\": \"promo15\"\n" +
                        "}");

        doNothing().when(facade).create(any(ActionDto.class));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).create(any(ActionDto.class));
    }

    @Test
    void getById() throws Exception {
        ActionDto dto = new ActionDto(100L);
        when(facade.findById(dto.getId())).thenReturn(Optional.of(dto));

        MockHttpServletRequestBuilder builder = get(path+"/100");

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(facade, Mockito.times(1)).findById(100L);
    }

    @Test
    void remove()throws Exception {
        doNothing().when(facade).delete(any(Long.class));

        MockHttpServletRequestBuilder builder = delete(path+"/100");
        mockMvc.perform(builder)
                .andExpect(status().isAccepted())
                .andExpect(content().string(""));

        verify(facade, Mockito.times(1)).delete(100L);
    }
}